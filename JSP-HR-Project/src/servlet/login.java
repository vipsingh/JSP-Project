package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class login
 */
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		
		
		String uname=request.getParameter("uname");
		String pass=request.getParameter("pass");
		
		try {
			ServletContext ctxt=getServletContext();
			//Getting the Driver class name from context parameter
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			//Getting the JDBC URL from context parameter
			String url=ctxt.getInitParameter("url");
			
			//Getting the DB Username, password & sqlstatement from servlet init parameter
			String dbuser=ctxt.getInitParameter("dbuser");
			String dbpass=ctxt.getInitParameter("dbpass");
			
			Connection con=DriverManager.getConnection(url, dbuser, dbpass);
			
		//out.println(con);		//Testing
		
		String query="select username,password from users where username=? AND password=?";
		
		PreparedStatement ps=con.prepareStatement(query);
		
		ps.setString(1, uname);
		ps.setString(2, pass);
		
		int count=ps.executeUpdate();

		if(count>0){
			
			//RequestDispatcher rd=request.getRequestDispatcher("/Viewproducts.html");
			//rd.include(request,response);
			HttpSession session=request.getSession();  
	        session.setAttribute("name",uname);
			response.sendRedirect("jobseekarafterlogin.jsp");
			/*Testing Purpose
			out.println("Record inserted count:"+count);
			out.println(""+con);
			*/
		}
		else{
			response.sendRedirect("error.html");
			out.println("Username & password does not Match");
		}
		con.close();
		
		
	} catch (SQLException e) {
		
		//response.sendRedirect("error.html");
		RequestDispatcher rd1=request.getRequestDispatcher("/index.html");
		rd1.include(request, response);;
		out.println("<center><b><i><u>"+e+"</u></i></b></center>");
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		
		out.println(e);
		e.printStackTrace();
	}

		
		doGet(request, response);
	}

}
