package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class postingjob
 */
public class postingjob extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public postingjob() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String id=request.getParameter("job_id");
		String title=request.getParameter("job_title");
		String company_address=request.getParameter("company_address");
		String branch=request.getParameter("branch");
		String technology=request.getParameter("technology");
		String job_description=request.getParameter("job_description");
		String experience=request.getParameter("experience");
		String salary=request.getParameter("salary");
		
		//out.println("Welcome "+uname);	//Testing
			
				try {
					//Class.forName("oracle.jdbc.driver.OracleDriver");
					
					ServletContext ctxt=getServletContext();
					//Getting the Driver class name from context parameter
					String driverClassName=ctxt.getInitParameter("driverClassName");
					Class.forName(driverClassName);
					//Getting the JDBC URL from context parameter
					String url=ctxt.getInitParameter("url");
					
					//Getting the DB Username, password & sqlstatement from servlet init parameter
					String dbuser=ctxt.getInitParameter("dbuser");
					String dbpass=ctxt.getInitParameter("dbpass");
					
					Connection con=DriverManager.getConnection(url, dbuser, dbpass);
				
				//out.println(con);			//Testing
				
				String query="insert into postingjob values(?,?,?,?,?,?,?,?)";
				
				PreparedStatement ps=con.prepareStatement(query);
				
				ps.setString(1, id);
				ps.setString(2, title);
				ps.setString(3, company_address);
				ps.setString(4, branch);
				ps.setString(5, technology);
				ps.setString(6, job_description);
				ps.setString(7, experience);
				ps.setString(8, salary);
				
				int count=ps.executeUpdate();

				if(count>0){
					
					// Alert And Redirect to login.html
					out.println("<html><script>alert('Data Save To Database SuccessFully.');window.location='index.jsp';</script></html>");
					
					
					/*//Include Register.html & print a msg 'register Successfully'
					RequestDispatcher rd=request.getRequestDispatcher("/Register.html");
					rd.include(request, response);
					out.println("<center><b><u><i>Registered SuccessFully<br>Please login using username & password</i></u></b></center>");
					
					*/
					
					//response.sendRedirect("Login.html"); //Direct Redirect to Login.html
					/*Testing Purpose
					out.println("Record inserted count:"+count);
					out.println(""+con);
					*/
				}
				else{
					
					out.println("Record not inserted");
				}
				con.close();
				
				
			} catch (SQLException e) {
				
				RequestDispatcher rd=request.getRequestDispatcher("/header.html");
				rd.include(request, response);
				out.println("<center><b><u><i>"+e+"</i></u></b></center>");
				
				//out.println("<center><b><u><i>Username Already Exists!<br>Please Select Another Username</i></u></b></center>");
				
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				out.println(e);
				e.printStackTrace();
			}
	
		doGet(request, response);
	}

}
