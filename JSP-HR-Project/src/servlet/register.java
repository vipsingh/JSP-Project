package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class register
 */
public class register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String name=request.getParameter("name");
		String address=request.getParameter("address");
		String education=request.getParameter("education");
		String college=request.getParameter("college");
		String field=request.getParameter("field");
		String percentage=request.getParameter("percentage");
		String title=request.getParameter("title");
		String skill1=request.getParameter("skill1");
		String skill2=request.getParameter("skill2");
		String skill3=request.getParameter("skill3");
		String password=request.getParameter("password");
		
		//out.println("Welcome "+uname);	//Testing
			
				try {
					//Class.forName("oracle.jdbc.driver.OracleDriver");
					
					ServletContext ctxt=getServletContext();
					//Getting the Driver class name from context parameter
					String driverClassName=ctxt.getInitParameter("driverClassName");
					Class.forName(driverClassName);
					//Getting the JDBC URL from context parameter
					String url=ctxt.getInitParameter("url");
					
					//Getting the DB Username, password & sqlstatement from servlet init parameter
					String dbuser=ctxt.getInitParameter("dbuser");
					String dbpass=ctxt.getInitParameter("dbpass");
					
					Connection con=DriverManager.getConnection(url, dbuser, dbpass);
				
				//out.println(con);			//Testing
				
				String query="insert into jobseekar values(?,?,?,?,?,?,?,?,?,?,?)";
				
				PreparedStatement ps=con.prepareStatement(query);
				
				ps.setString(1, name);
				ps.setString(2, address);
				ps.setString(3, education);
				ps.setString(4, college);
				ps.setString(5, field);
				ps.setString(6, percentage);
				ps.setString(7, title);
				ps.setString(8, skill1);
				ps.setString(9, skill2);
				ps.setString(10, skill3);
				ps.setString(11, password);
				
				int count=ps.executeUpdate();

				if(count>0){
					
					// Alert And Redirect to login.html
					out.println("<html><script>alert('Registered SuccessFully.Please login using username & password.');window.location='jobseekarui.jsp';</script></html>");
					
	
				}
				else{
					
					out.println("Record not inserted");
				}
				con.close();
				
				
			} catch (SQLException e) {
				
				RequestDispatcher rd=request.getRequestDispatcher("/header.html");
				rd.include(request, response);
				out.println("<center><b><u><i>"+e+"</i></u></b></center>");
				
				//out.println("<center><b><u><i>Username Already Exists!<br>Please Select Another Username</i></u></b></center>");
				
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				out.println(e);
				e.printStackTrace();
			}
	
		
		doGet(request, response);
	}

}
