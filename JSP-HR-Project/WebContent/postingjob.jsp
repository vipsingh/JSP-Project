<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% 
		String id=request.getParameter("job_id");
		String title=request.getParameter("job_title");
		String company_address=request.getParameter("company_address");
		String branch=request.getParameter("branch");
		String technology=request.getParameter("technology");
		String job_description=request.getParameter("job_description");
		String experience=request.getParameter("experience");
		String salary=request.getParameter("salary");
		
		try {
			ServletContext ctxt=getServletContext();
			//Getting the Driver class name from context parameter
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			//Getting the JDBC URL from context parameter
			String url=ctxt.getInitParameter("url");
			
			//Getting the DB Username, password & sqlstatement from servlet init parameter
			String dbuser=ctxt.getInitParameter("dbuser");
			String dbpass=ctxt.getInitParameter("dbpass");
			
			Connection con=DriverManager.getConnection(url, dbuser, dbpass);
			
		//out.println(con);		//Testing
		
		String query="insert into recruiter1 values(?,?,?,?,?,?,?,?)";
		
		PreparedStatement ps=con.prepareStatement(query);
		
		ps.setString(1, id);
		ps.setString(2, title);
		ps.setString(3, company_address);
		ps.setString(4, branch);
		ps.setString(5, technology);
		ps.setString(6, job_description);
		ps.setString(7, experience);
		ps.setString(8, salary);
		
		int count=ps.executeUpdate();

		if(count>0){
			
			response.sendRedirect("index.jsp");
		}
		else{
			response.sendRedirect("header.html");
			out.println("Username & password does not Match");
		}
		con.close();
		
		
	} catch (SQLException e) {
		
		RequestDispatcher rd1=request.getRequestDispatcher("index.jsp");
		rd1.include(request, response);;
		out.println("<center><b><i><u>"+e+"</u></i></b></center>");
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		
		out.println(e);
		e.printStackTrace();
	}
	%>

</body>
</html>