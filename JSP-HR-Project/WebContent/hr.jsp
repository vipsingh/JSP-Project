<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% 
		String uname=request.getParameter("name");
		String pass=request.getParameter("pwd");
		
		try {
			ServletContext ctxt=getServletContext();
			//Getting the Driver class name from context parameter
			String driverClassName=ctxt.getInitParameter("driverClassName");
			Class.forName(driverClassName);
			//Getting the JDBC URL from context parameter
			String url=ctxt.getInitParameter("url");
			
			//Getting the DB Username, password & sqlstatement from servlet init parameter
			String dbuser=ctxt.getInitParameter("dbuser");
			String dbpass=ctxt.getInitParameter("dbpass");
			
			Connection con=DriverManager.getConnection(url, dbuser, dbpass);
			
		//out.println(con);		//Testing
		
		String query="select uname,password from recruiter where uname=? AND password=?";
		
		PreparedStatement ps=con.prepareStatement(query);
		
		ps.setString(1, uname);
		ps.setString(2, pass);
		
		int count=ps.executeUpdate();

		if(count>0){
			
	        session.setAttribute("name",uname);
			response.sendRedirect("Recruiterdomain.jsp");
		}
		else{
			response.sendRedirect("header.html");
			out.println("Username & password does not Match");
		}
		con.close();
		
		
	} catch (SQLException e) {
		
		RequestDispatcher rd1=request.getRequestDispatcher("index.jsp");
		rd1.include(request, response);;
		out.println("<center><b><i><u>"+e+"</u></i></b></center>");
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		
		out.println(e);
		e.printStackTrace();
	}
	%>

</body>
</html>